<?php

if (version_compare(phpversion(), '7.2', '<')) {
    die('Ошибка! Минимальная версия PHP 7.2.');
}

define('ROOT', dirname(__DIR__));

require 'core/Autoloader.php';
\core\Autoloader::register();

$configPath = \core\Config::CONFIG_PATH;
$sampleConfigPath = str_replace('.php', '.php.sample', $configPath);

if (!file_exists($configPath)) {
    copy($sampleConfigPath, $configPath);
}

$db = \db\DB::Instance()->ConnectToDb();

\db\TableCreator::Instance()->CreateUserTable($db);
\db\TableCreator::Instance()->CreateImageTable($db);
\db\TableCreator::Instance()->CreateCommentTable($db);
\db\TableCreator::Instance()->CreateFavoritesTable($db);
\db\TableCreator::Instance()->CreateCategoryTable($db);
\db\TableCreator::Instance()->CreateLikesTable($db);

\db\CategoryDB::Instance()->CreateCategories(
    [
        'volkswagen', 'собачки', 'котики', 'автомобили', 'спортивные-авто', 'девушки', 'украина',
        'житомир', 'mercedes', 'германия', 'орехи', 'еда'
    ]
);

echo "Приложение успешно установлено!\n";
