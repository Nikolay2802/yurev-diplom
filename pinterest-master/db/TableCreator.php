<?php

namespace db;

class TableCreator
{
    private static $instance;

    public static function Instance()
    {
        if (self::$instance == null)
            self::$instance = new self();
        return self::$instance;
    }

    private function __construct()
    {
    }

    public function CreateCommentTable($dbHandle)
    {
        $query = "CREATE TABLE `comments` (
            `id` INT PRIMARY KEY AUTO_INCREMENT,
            `image_id` INT NOT NULL,
            `parent_id` INT NULL,
            `user_id` INT NOT NULL,
            `comment_text` VARCHAR(511) NOT NULL,
            `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP(),
            FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
            FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE,
            FOREIGN KEY (parent_id) REFERENCES comments (id) ON DELETE CASCADE
        ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }

    public function CreateUserTable($dbHandle)
    {
        $query = "CREATE TABLE `user` (
            `id` INT PRIMARY KEY AUTO_INCREMENT,
            `login` VARCHAR(25) NOT NULL,
            `password` VARCHAR(25) NOT NULL,
            `avatar_path` VARCHAR(150) NOT NULL,
            `name` VARCHAR(150) NULL,
            `surname` VARCHAR(150) NULL,
            `email` VARCHAR(150) NULL,
            `city` VARCHAR(150) NULL,
            `country` VARCHAR(150) NULL,
            `age` VARCHAR(150) NULL,
            UNIQUE(`login`)
        ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }

    public function CreateImageTable($dbHandle)
    {
        $query = "CREATE TABLE `image` (
            `id` INT PRIMARY KEY AUTO_INCREMENT,
            `title` VARCHAR(50) NOT NULL,
            `description` VARCHAR(4096) NOT NULL,
            `user_id` INT NOT NULL,
            `date` DATE NOT NULL,
            `views_count` INT NOT NULL DEFAULT 0,
            `image_path` VARCHAR(150) NOT NULL,
            `category_id` INT NOT NULL,
            FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE
         ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }

    public function CreateCategoryTable($dbHandle)
    {
        $query = "CREATE TABLE `category` (
            `id` INT PRIMARY KEY AUTO_INCREMENT ,
            `name` VARCHAR(50) NOT NULL,
            UNIQUE (`name`)
        ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }

    public function CreateFavoritesTable($dbHandle)
    {
        $query = "CREATE TABLE `favorites` (
            `id` INT PRIMARY KEY AUTO_INCREMENT,
            `user_id` INT NOT NULL,
            `image_id` INT NOT NULL,
            UNIQUE KEY `user_id-image_id` (`user_id`,`image_id`),
            FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
            FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE
        ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }

    public function CreateLikesTable($dbHandle)
    {
        $query = "CREATE TABLE `likes` (
            `id` INT PRIMARY KEY AUTO_INCREMENT,
            `user_id` INT NOT NULL,
            `image_id` INT NOT NULL,
            UNIQUE KEY `user_id-image_id` (`user_id`,`image_id`),
            FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE,
            FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE
        ) ENGINE = InnoDB";
        $dbHandle->exec($query);
    }
}
