<?php

namespace db;

class FavoritesDB extends DB
{
    const FAVORITES_TABLE_NAME = "favorites";

    public function SetFavorite($user_id, $image_id)
    {
        $tableName = self::FAVORITES_TABLE_NAME;
        $stmt = $this->handle->prepare("INSERT INTO $tableName (`id`, `user_id`, `image_id`)
                                VALUES (NULL, ?, ?)");
        return $stmt->execute(array($user_id, $image_id));
    }

    public function GetFavorites($user_id)
    {
        $tableName = self::FAVORITES_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT `image_id` FROM $tableName WHERE `user_id`=?");
        $stmt->execute(array($user_id));
        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }

    public function Remove($user_id, $image_id)
    {
        $tableName = self::FAVORITES_TABLE_NAME;
        $stmt = $this->handle->prepare("DELETE FROM $tableName WHERE `user_id`=? && `image_id`=?");
        $stmt->execute(array($user_id, $image_id));
    }

    public function isIsset($user_id, $image_id)
    {
        $tableName = self::FAVORITES_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $tableName WHERE `user_id`=? && `image_id`=?");
        $stmt->execute(array($user_id, $image_id));
        return !empty($stmt->fetchAll(\PDO::FETCH_COLUMN));
    }

    public function getInFavoriteCount($imageId)
    {
        $stmt = $this->handle->prepare('SELECT COUNT(*) FROM ' . self::FAVORITES_TABLE_NAME . ' WHERE image_id = ?');
        $stmt->execute([$imageId]);
        return $stmt->fetch()[0];
    }
}