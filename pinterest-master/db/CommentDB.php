<?php

namespace db;

class CommentDB extends DB
{
    const COMMENTS_TABLE_NAME = 'comments';

    public function GetComments($image_id)
    {
        $tableName = static::COMMENTS_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $tableName WHERE `image_id`=? ORDER BY `date`");
        $stmt->execute(array($image_id));
        return $stmt->fetchAll();
    }

    public function AddComment($userId, $parent_id, $image_id, $comment)
    {
        $tableName = static::COMMENTS_TABLE_NAME;
        $stmt = $this->handle->prepare("INSERT INTO $tableName (`image_id`, `parent_id`, `user_id`, `comment_text`)
          VALUES (?, ?, ?, ?)");
        $stmt->execute([$image_id, $parent_id, $userId, $comment]);
    }

    public function DeleteComment($comment_id)
    {
        $tableName = static::COMMENTS_TABLE_NAME;
        $stmt = $this->handle->prepare("DELETE FROM $tableName WHERE `id`=?");
        $stmt->execute(array($comment_id));
    }

    public function getCommentsCount($imageId)
    {
        $stmt = $this->handle->prepare('SELECT COUNT(*) FROM ' . self::COMMENTS_TABLE_NAME . ' WHERE image_id = ?');
        $stmt->execute([$imageId]);
        return $stmt->fetch()[0];
    }
}
