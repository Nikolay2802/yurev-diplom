<?php

namespace db;

class CategoryDB extends DB
{
    const CATEGORY_TABLE_NAME = "category";

    public function GetAllCategories()
    {
        $tableName = static::CATEGORY_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $tableName");
        $stmt->execute();
        $array = $stmt->fetchAll();

        $result = array();
        foreach ($array as $category)
            $result[$category['id']] = $category['name'];
        return $result;
    }

    public function AddCategory($categoryName)
    {
        $tableName = static::CATEGORY_TABLE_NAME;
        $stmt = $this->handle->prepare("INSERT INTO $tableName (`id`, `name`) VALUES (NULL, ?)");
        $result = $stmt->execute(array(mb_strtolower($categoryName)));
        return $result;
    }

    public function GetCategoryIdByName($name)
    {
        $tableName = static::CATEGORY_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT id FROM $tableName WHERE name = ?");
        $stmt->execute([$name]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);
        return $result;
    }

    public function CreateCategories($categoriesArray)
    {
        $tableName = static::CATEGORY_TABLE_NAME;
        foreach ($categoriesArray as $category) {
            $stmt = $this->handle->prepare("INSERT INTO $tableName (`id`, `name`) VALUES (NULL, ?)");
            $stmt->execute([mb_strtolower($category)]);
        }
        return true;
    }
}