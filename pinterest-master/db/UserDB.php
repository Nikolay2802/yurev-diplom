<?php

namespace db;

use models\User;

class UserDB extends DB
{
    const USER_TABLE_NAME = "user";

    public function WriteUser(User $user)
    {
        $tableName = static::USER_TABLE_NAME;
        $stmt = $this->handle->prepare("INSERT INTO $tableName
				(`id`, `login`, `password`, `avatar_path`) VALUES (NULL, ?, ?, ?)");
        $stmt->execute(array($user->login, $user->password, $user->avatarPath));
        return true;
    }

    public function UpdateUser(User $user)
    {
        $tableName = static::USER_TABLE_NAME;
        $stmt = $this->handle->prepare(
            "UPDATE $tableName SET name = ?, surname = ?, email = ?, city = ?, country = ?, age = ? WHERE login = ?"
        );
        $res = $stmt->execute([$user->name, $user->surname, $user->email, $user->city, $user->country, $user->age, $user->login]);
        return true;
    }

    public function GetUserByLogin($login)
    {
        return $this->getUser('login', $login);
    }

    public function DeleteUser($user)
    {
        $tableName = static::USER_TABLE_NAME;

        $stmt = $this->handle->prepare("DELETE FROM $tableName WHERE id=?");
        $result = $stmt->execute([$user->id]);
        return $result;
    }

    public function LoginExist($login)
    {
        $tableName = static::USER_TABLE_NAME;

        $stmt = $this->handle->prepare("SELECT COUNT(1) FROM $tableName WHERE login=?");
        $stmt->execute(array($login));
        $res = $stmt->fetch();
        return $res[0] > 0 ? true : false;
    }

    public function GetUserById($id)
    {
        return $this->getUser('id', $id);
    }

    public function getUser($column, $value)
    {
        $tableName = static::USER_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $tableName WHERE $column=?");
        $stmt->execute([$value]);
        $string = $stmt->fetch();

        return static::prepareUser($string);
    }

    protected static function prepareUser($array)
    {
        $user = new User($array['id'], $array['login'], $array['password'], $array['avatar_path']);
        $user->name = $array['name'];
        $user->surname = $array['surname'];
        $user->email = $array['email'];
        $user->city = $array['city'];
        $user->country = $array['country'];
        $user->age = $array['age'];
        return $user;
    }
}