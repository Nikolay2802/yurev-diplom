<?php

namespace db;

use core\Config;

class DB
{
    protected static $instance;

    public static function Instance()
    {
        if (self::$instance == null)
            self::$instance = new static();
        else if (!self::$instance instanceof static)
            self::$instance = new static();
        return self::$instance;
    }

    /**
     * @var \PDO
     */
    protected $handle;

    /**
     * @var array|null
     */
    protected $dbConfig;

    private function __construct()
    {
        $this->dbConfig = Config::getConfigByKey('db');
        self::ConnectToDb();
    }

    public function ConnectToDb():\PDO
    {
        try {
            $this->handle = new \PDO('mysql:host=' . $this->dbConfig['host'] . ';dbname=' . $this->dbConfig['name'], $this->dbConfig['user'], $this->dbConfig['password']);
        } catch (\PDOException $ex) {
            switch ($ex->getCode()) {
                case 1049:
                    self::CreateDb();
                    break;
                case 1045:
                    exit("Не верный логин или пароль при подключении к БД в config/app.php\n");
                    break;
                default:
                    throw new \PDOException($ex);
            }
        }
        return $this->handle;
    }

    private function CreateDb()
    {
        $this->handle = new \PDO('mysql:host=' . $this->dbConfig['host'], $this->dbConfig['user'], $this->dbConfig['password']);
        $this->handle->exec("CREATE DATABASE IF NOT EXISTS " . $this->dbConfig['name']);
        self::ConnectToDb();
    }
}