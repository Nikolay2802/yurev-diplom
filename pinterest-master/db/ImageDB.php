<?php

namespace db;

use models\Category;
use models\Image;

class ImageDB extends DB
{
    const IMAGE_TABLE_NAME = 'image';

    public function GetAllImages()
    {
        if (self::Instance()->GetLastImageId() == 0)
            return null;

        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $table");
        $stmt->execute();
        $result = $stmt->fetchAll();

        $images = array();
        foreach ($result as $row) {
            $categoryName = Category::GetCategoryName($row['category_id']);
            $images[] = new Image($row['id'], $row['title'], $row['description'], $row['user_id'],
                $row['date'], $row['image_path'], $categoryName);
        }
        return $images;
    }

    public function GetImagesLike($name)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $table WHERE `title`LIKE ?");
        $stmt->execute(array('%' . $name . '%'));
        return $stmt->fetchAll();
    }

    public function UpdateImageField($imageId, $fieldName, $fieldValue)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("UPDATE $table SET $fieldName = ? WHERE id = ?");
        $result = $stmt->execute([$fieldValue, $imageId]);
        return $result;
    }

    public function GetImageById($id)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $table WHERE id=?");
        $stmt->execute(array($id));
        $result = $stmt->fetchAll();
        $categoryName = Category::GetCategoryName($result[0]['category_id']);
        return new Image($result[0]['id'], $result[0]['title'], $result[0]['description'], $result[0]['user_id'],
            $result[0]['date'], $result[0]['image_path'], $categoryName);
    }

    public function WriteImage(Image $image)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("INSERT INTO $table
				(`id`, `title`, `description`, `user_id`, `date`, `image_path`, `category_id`) 
				VALUES (NULL, ?, ?, ?, ?, ?, ?)");
        $stmt->execute(array($image->title, $image->description,
            $image->user_id, $image->date, $image->image_path, Category::GetCategoryId($image->category_name)));
        return true;
    }

    public function GetLastImageId()
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->query("SELECT MAX(id) FROM $table");
        if (!$stmt)
            return 0;
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result[0][0];
    }

    public function GetImagesByCategoryId($id)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $table WHERE `category_id`=?");
        $stmt->execute(array($id));
        return $stmt->fetchAll();
    }

    public function GetUserImages($user_id)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("SELECT * FROM $table WHERE `user_id`=?");
        $stmt->execute([$user_id]);
        return $stmt->fetchAll();
    }

    public function UpdateImage($imageId, $infoArray)
    {
        if ($categoryName = $infoArray['category_name']) {
            $categoryId = CategoryDB::Instance()->GetCategoryIdByName($categoryName);
        }
        if (!$categoryId) {
            CategoryDB::Instance()->AddCategory($categoryName);
            $categoryId = CategoryDB::Instance()->GetCategoryIdByName($categoryName);
        }
        $categoryId = $categoryId['id'];

        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("UPDATE $table SET title = ?, description = ?, category_id = ? WHERE id = ?");
        $result = $stmt->execute([
            $infoArray['title'] ?? '',
            $infoArray['description'] ?? '',
            $categoryId,
            $imageId
        ]);

        return $result;
    }

    public function incrementViewCount($imageId)
    {
        $stmt = $this->handle->prepare('UPDATE ' . self::IMAGE_TABLE_NAME . ' SET views_count = views_count + 1 WHERE id = ?');
        $stmt->execute([$imageId]);
    }

    public function getViewCount($imageId)
    {
        $stmt = $this->handle->prepare('SELECT views_count FROM ' . self::IMAGE_TABLE_NAME . ' WHERE id = ?');
        $stmt->execute([$imageId]);
        return $stmt->fetch()[0];
    }

    public function DeleteImageById($imageId)
    {
        $table = static::IMAGE_TABLE_NAME;
        $stmt = $this->handle->prepare("DELETE FROM $table WHERE id = ?");
        $stmt->execute([$imageId]);
    }
}