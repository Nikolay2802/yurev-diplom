<?php

namespace db;

class LikesDB extends DB
{
    const LIKE_TABLE_NAME = 'likes';

    public function likeImage($userId, $imageId)
    {
        $stmt = $this->handle->prepare('INSERT INTO ' . self::LIKE_TABLE_NAME . ' VALUES(NULL, ?,?)');
        $result = $stmt->execute([$userId, $imageId]);
        return $result;
    }

    public function checkIfUserLikeImage($userId, $imageId)
    {
        $stmt = $this->handle->prepare('SELECT COUNT(*) FROM ' . self::LIKE_TABLE_NAME . ' WHERE user_id = ? and image_id = ?');
        $stmt->execute([$userId, $imageId]);
        return $stmt->fetch()[0] > 0;
    }

    public function getImageLikesCount($imageId)
    {
        $stmt = $this->handle->prepare('SELECT COUNT(*) FROM ' . self::LIKE_TABLE_NAME . ' WHERE image_id = ?');
        $stmt->execute([$imageId]);
        return $stmt->fetch()[0];
    }

    public function dislikeImage($userId, $imageId)
    {
        $stmt = $this->handle->prepare('DELETE FROM ' . self::LIKE_TABLE_NAME . ' WHERE user_id = ? and image_id = ?');
        $result = $stmt->execute([$userId, $imageId]);
        return $result;
    }
}
