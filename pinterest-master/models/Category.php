<?php

namespace models;

use db\CategoryDB;

class Category
{
    public static function GetCategoryName($categoryId)
    {
        if (!is_numeric($categoryId))
            return null;
        $categories = CategoryDB::Instance()->GetAllCategories();
        return $categories[$categoryId];
    }

    public static function GetCategoryId($categoryName, $needToAdd = true)
    {
        $categories = CategoryDB::Instance()->GetAllCategories();   // TODO: переписать
        foreach ($categories as $id => $category)
            if ($category == $categoryName)
                return $id;
        if ($needToAdd) {
            CategoryDB::Instance()->AddCategory($categoryName);
            return self::GetCategoryId($categoryName);
        } else
            return -1;
    }

    public static function GetAllCategories()
    {
        return CategoryDB::Instance()->GetAllCategories();
    }
}