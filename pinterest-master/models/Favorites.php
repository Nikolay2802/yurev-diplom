<?php

namespace models;

use db\FavoritesDB;

class Favorites
{
    public static function Add(User $user, $image_id)
    {
        return FavoritesDB::Instance()->SetFavorite($user->id, $image_id);
    }

    public static function GetFavorites(User $user)
    {
        return FavoritesDB::Instance()->GetFavorites($user->id);
    }

    public static function Remove(User $user, $image_id)
    {
        FavoritesDB::Instance()->Remove($user->id, $image_id);
    }

    public static function isIsset(User $user, $image_id)
    {
        return FavoritesDB::Instance()->isIsset($user->id, $image_id);
    }
}