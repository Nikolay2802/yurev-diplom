<?php

namespace models;

use db\CommentDB;

class Comment
{
    public $id;
    public $image_id;
    public $parent_id;
    public $user_login;
    public $comment_text;
    public $date;

    protected static $comments;

    public function __construct($id, $parent_id, $image_id, $user_login, $comment_text, $date)
    {
        $this->id = $id;
        $this->image_id = $image_id;
        $this->parent_id = $parent_id;
        $this->user_login = $user_login;
        $this->comment_text = $comment_text;
        $this->date = $date;
    }

    public static function GetCommentsByImageId($image_id)
    {
        $array = CommentDB::Instance()->GetComments($image_id);

        foreach ($array as $item) {
            self::$comments[$item['id']] = new Comment($item['id'], $item['parent_id'], $item['image_id'], User::GetUserById($item['user_id'])->login, $item['comment_text'], $item['date']);
        }

        return self::$comments;
    }

    public static function WriteComment(User $user, $image_id, $comment, $parentId = null)
    {
        CommentDB::Instance()->AddComment($user->id, $parentId, $image_id, $comment);
    }

    public static function RemoveComment($comment_id)
    {
        CommentDB::Instance()->DeleteComment($comment_id);
    }

    public static function GetAllSubComments($parentId)
    {
        $subcomments = [];

        foreach (self::$comments as $comment) {
            if ($comment->parent_id == $parentId) {
                $subcomments[] = $comment;
            }
        }

        return $subcomments;
    }
}