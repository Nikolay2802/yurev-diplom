<?php

namespace models;

use core\ActiveUser;
use db\LikesDB;

class Likes
{
    public static function likeImage($imageId)
    {
        if (self::isActiveUserLikedImage($imageId)) {
            self::activeUserDislikeImage($imageId);
        } else {
            self::activeUserLikeImage($imageId);
        }
        return self::getLikesCountForImage($imageId);
    }

    public static function activeUserLikeImage($imageId)
    {
        $user = ActiveUser::GetActiveUser();
        $result = LikesDB::Instance()->likeImage($user->id, $imageId);
        return (bool)$result;
    }

    public static function activeUserDislikeImage($imageId)
    {
        $user = ActiveUser::GetActiveUser();
        $result = LikesDB::Instance()->dislikeImage($user->id, $imageId);
        return (bool)$result;
    }

    public static function isActiveUserLikedImage($imageId)
    {
        return LikesDB::Instance()->checkIfUserLikeImage(ActiveUser::GetActiveUser()->id, $imageId);
    }

    public static function getLikesCountForImage($imageId)
    {
        return LikesDB::Instance()->getImageLikesCount($imageId);
    }
}
