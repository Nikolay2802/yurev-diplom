<?php

namespace models;

use core\ActiveUser;
use core\View;

class Mail
{
    const MAIL_NOTIFICATION_TEMPLATE = 'image_comment_notification';

    public static function sendMailNotificationToImageOwner($imageId)
    {
        $image = Image::GetImage($imageId);
        if (!$image) {
            throw new \exception('Image not found');
        }

        if (ActiveUser::GetActiveUser()->id == $image->user_id) {
            return true;
        }

        $user = User::GetUserById($image->user_id);

        $view = new View('image_comment_notification', ['user' => $user, 'image_url' => $_SERVER['HTTP_HOST'] . '/images/' . $image->id], false);
        $view->SetController(new \controllers\ImageController());
        $html = $view->fetch();

        $email = $user->email;
        if (!$email) {
            return false;
        }

        $headers = 'From: pinterest2@gmail.com' . "\r\n";
        $headers .= 'Reply-To: pinterest2@gmail.com' . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        $result = mail($email, 'Новый комментарий к фото', $html, $headers);
        return $result;
    }
}
