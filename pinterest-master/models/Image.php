<?php

namespace models;

use db\ImageDB;

class Image
{
    public $id;
    public $title;
    public $description;
    public $user_id;
    public $date;
    public $image_path;
    public $category_name;

    public $owner_login;

    public function __construct($id, $title, $description, $user_id, $date, $image_path, $category_name)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->user_id = $user_id;
        $this->date = $date;
        $this->image_path = $image_path;
        $this->category_name = $category_name;
        $this->owner_login = \models\User::GetUserById($user_id)->login;
    }

    public static function GetImage($id)
    {
        return ImageDB::Instance()->GetImageById($id);
    }

    public static function GetAllImages()
    {
        return ImageDB::Instance()->GetAllImages();
    }

    public static function WriteImage(Image $image)
    {
        ImageDB::Instance()->WriteImage($image);
    }

    public static function UpdateImagePath($imageId, $newImagePath)
    {
        return ImageDB::Instance()->UpdateImageField($imageId, 'image_path', $newImagePath);
    }

    public static function FindImages($name)
    {
        $array = ImageDB::Instance()->GetImagesLike($name);

        $images = array();
        foreach ($array as $image) {
            $images[] = new Image($image['id'], $image['title'], $image['description'], $image['user_id'],
                $image['date'], $image['image_path'], Category::GetCategoryName($image['category_id']));
        }
        return $images;
    }

    public static function GetLastId()
    {
        return ImageDB::Instance()->GetLastImageId();
    }

    public static function GetImagesInCategory($category_name)
    {
        $id = Category::GetCategoryId($category_name, false);
        return self::GetImagesByCategoryId($id);
    }

    private static function GetImagesByCategoryId($id)
    {
        $array = ImageDB::Instance()->GetImagesByCategoryId($id);
        $images = array();
        foreach ($array as $image) {
            $images[] = new Image($image['id'], $image['title'], $image['description'], $image['user_id'],
                $image['date'], $image['image_path'], Category::GetCategoryName($image['category_id']));
        }
        return $images;
    }

    public static function GetUserImages($userId)
    {
        $array = ImageDB::Instance()->GetUserImages($userId);
        $images = array();
        foreach ($array as $image) {
            $images[] = new Image($image['id'], $image['title'], $image['description'], $image['user_id'],
                $image['date'], $image['image_path'], Category::GetCategoryName($image['category_id']));
        }
        return $images;
    }

    public static function CheckImageOwner(Image $image, User $user)
    {
        return $image->user_id == $user->id;
    }

    public static function UpdateImage($imageId, array $imageInfo)
    {
        return ImageDB::Instance()->UpdateImage($imageId, $imageInfo);
    }

    public static function DeleteImageById($imageId)
    {
        ImageDB::Instance()->DeleteImageById($imageId);
    }

    public static function increaseViewsCount($imageId)
    {
        ImageDB::Instance()->incrementViewCount($imageId);
    }
}