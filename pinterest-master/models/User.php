<?php

namespace models;

use db\UserDB;

class User
{
    public $id;

    public $login;
    public $password;
    public $avatarPath;

    public $name;
    public $surname;
    public $email;
    public $city;
    public $country;
    public $age;

    public function __construct($id, $login, $password, $avatarPath)
    {
        $this->id = $id;
        $this->login = mb_strtolower($login);
        $this->password = $password;
        $this->avatarPath = $avatarPath;
    }

    public static function GetUserByLogin($login)
    {
        return UserDB::Instance()->GetUserByLogin($login);
    }

    public static function UpdateUserInfo(User $user, $infoArray)
    {
        $user->name = $infoArray['name'];
        $user->surname = $infoArray['surname'];
        $user->email = $infoArray['email'];
        $user->city = $infoArray['city'];
        $user->country = $infoArray['country'];
        $user->age = $infoArray['age'];
        UserDB::Instance()->UpdateUser($user);
    }

    public static function AddUserToDb(User $user)
    {
        return UserDB::Instance()->WriteUser($user);
    }

    public static function LoginExistInDb($login)
    {
        return UserDB::Instance()->LoginExist($login);
    }

    public static function GetUserById($id)
    {
        return UserDB::Instance()->GetUserById($id);
    }
}