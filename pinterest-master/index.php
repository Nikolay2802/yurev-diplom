<?php

define('ROOT', dirname(__FILE__));
define('IMAGES_PATH', '/var/files/users_images/');
define('BASIC_AVATAR_PATH', IMAGES_PATH . '/basic_user.png');

session_start();

require 'core/Autoloader.php';
\core\Autoloader::register();

\core\ActiveUser::CheckActiveUser();

$router = new \core\Router();
$router->Run();
