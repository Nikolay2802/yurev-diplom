<?php

namespace ViewModel;

class ImageStatistics
{
    private $image;

    public function __construct($imageId)
    {
        $this->image = \models\Image::GetImage($imageId);
        if (!$this->image) {
            throw new \exception('Image not found!');
        }
    }

    public function getViewsCount()
    {
        return \db\ImageDB::Instance()->getViewCount($this->image->id);
    }

    public function getLikesCount()
    {
        return \models\Likes::getLikesCountForImage($this->image->id);
    }

    public function getCommentsCount()
    {
        return \db\CommentDB::Instance()->getCommentsCount($this->image->id);
    }

    public function getInFavoriteCount()
    {
        return \db\FavoritesDB::Instance()->getInFavoriteCount($this->image->id);
    }
}
