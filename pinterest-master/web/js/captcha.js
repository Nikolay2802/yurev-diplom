var digits = {
    0: [[0, 1, 1, 1],
        [0, 1, 0, 1],
        [0, 1, 0, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1]
    ],
    1: [[0, 0, 1, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 1, 1, 1]
    ],
    2: [[0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 0, 1, 0],
        [0, 1, 0, 0],
        [0, 1, 1, 1]
    ],
    3: [[0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 1, 1, 1]
    ],
    4: [[0, 1, 0, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 0, 0, 1]
    ],
    5: [[0, 1, 1, 1],
        [0, 1, 0, 0],
        [0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 1, 1, 1]
    ],
    6: [[0, 1, 1, 1],
        [0, 1, 0, 0],
        [0, 1, 1, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1]
    ],
    7: [[0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0]
    ],
    8: [[0, 1, 1, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1]
    ],
    9: [[0, 1, 1, 1],
        [0, 1, 0, 1],
        [0, 1, 1, 1],
        [0, 0, 0, 1],
        [0, 1, 1, 1]
    ]
}
const ROWS = 5;
const COLS = 5;
let main = document.getElementById('main');

let savedNumber = 0;

function createNumber(number) {
    let body = document.createElement('div');
    body.style.display = 'inline-block';
    const numbers = digits[+number];
    main.style.display = 'inline-block';
    for (let i = 0; i < ROWS; i++) {
        let row = createROW();
        for (let j = 0; j < COLS; j++) {
            let span = createspan('#fff');
            if (numbers[i][j] === 1) span = createspan('#9B111E');

            row.appendChild(span);
        }
        body.appendChild(row);
    }
    return body;
}

function createNumbers(number = null) {
    if (main.hasChildNodes()) {
        while (main.firstChild) {
            main.removeChild(main.firstChild);
        }
    }
    value = number ? number.toString() : String(document.getElementById('input').value).split('');
    savedNumber = value;
    for (var i = 0; i < value.length; i++) {
        main.appendChild(createNumber(value[i]))
    }

}

function createROW() {
    return document.createElement('div');
}

function createspan(color) {
    const span = document.createElement('span');
    span.style.backgroundColor = color;
    span.style.display = 'inline-block';
    span.style.width = 20 + 'px';
    span.style.height = 20 + 'px';
    return span;
}

function generateRandomNumbers() {
    let num = Math.floor(Math.random() * 100) + 1;
    createNumbers(num);
}

function checkCaptcha() {
    var inputNum = document.getElementById('captchaInput').value;
    var captchaResultBlock = document.getElementById('captchaResult');
    if (inputNum.toString() == savedNumber.toString()) {
        document.getElementById('captchaResult').value = 1;
    }
}
