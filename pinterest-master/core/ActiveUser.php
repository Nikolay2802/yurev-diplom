<?php

namespace core;

use models\User;

class ActiveUser
{
    private static $activeUser;

    public static function SetActiveUser(User $user)
    {
        self::$activeUser = $user;
        $_SESSION['login'] = self::$activeUser->login;
    }

    public static function GetActiveUser(): ?User
    {
        return self::$activeUser;
    }

    public static function CheckActiveUser()
    {
        if (isset($_SESSION['login'])) {
            $login = $_SESSION['login'];
            $user = User::GetUserByLogin($login);
            self::SetActiveUser($user);
            return true;
        }
        self::$activeUser = null;
        return false;
    }

    public static function DeleteActiveUser()
    {
        session_destroy();
        self::$activeUser = null;
    }

}