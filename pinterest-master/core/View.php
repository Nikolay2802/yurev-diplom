<?php

namespace core;

class View
{
    private $viewName;
    private $viewPath;
    private $layoutPath;
    private $needLayout;
    private $model;

    public function __construct($viewName, $model = null, $needLayout = true)
    {
        $this->viewName = $viewName;
        $this->model = $model;
        $this->needLayout = $needLayout;
    }

    public function SetController($controllerObject)
    {
        $this->layoutPath = $this->getLayoutPath($controllerObject->getLayout());
        $this->viewPath = $this->getViewPath(get_class($controllerObject));
    }

    public function Show()
    {
        echo $this->fetch();
    }

    public function fetch()
    {
        ob_start();
        $model = $this->model;  // это используется при выводе view
        if ($this->layoutPath != null && $this->needLayout) {
            require_once $this->layoutPath;
        } else {
            require_once $this->viewPath;
        }
        $html = ob_get_clean();
        return $html;
    }

    private function getLayoutPath($layoutName)
    {
        if ($layoutName == null)
            return null;
        return ROOT . '/views/layouts/' . $layoutName . '.phtml';
    }

    private function getViewPath($controllerName)
    {
        $name = str_replace("Controller", '', $controllerName);
        $name = substr($name, strripos($name, '\\') + 1);
        $name = lcfirst($name);
        return ROOT . '/views/' . $name . '/' . $this->viewName . '.phtml';
    }
}