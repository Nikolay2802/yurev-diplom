<?php

namespace core;

class Autoloader
{
    public static function register()
    {
        spl_autoload_register(function ($className) {
            $classPath = str_replace('\\', '/', $className) . '.php';
            require ROOT . '/' . $classPath;
        });
    }
}
