<?php

namespace core;

interface ControllerInterface
{
    public function getLayout();
}