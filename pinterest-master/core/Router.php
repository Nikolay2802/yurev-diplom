<?php

namespace core;

class Router
{
    private $routes;

    public function __construct()
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
    }

    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI']))
            return trim($_SERVER['REQUEST_URI'], '/');
    }

    public function Run()
    {
        $uri = urldecode($this->getURI());
        foreach ($this->routes as $uriPattern => $path) {
            if (preg_match("~$uriPattern~", $uri)) {
                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);

                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = "\\controllers\\" . ucfirst($controllerName);

                $actionName = "action" . ucfirst(array_shift($segments));
                $parameters = $segments;

                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);

                if ($result != null) {
                    $result->SetController($controllerObject);
                    $result->Show();
                    break;
                } else {
                    $error404 = new \controllers\Error404Controller();
                    $view = $error404->actionIndex();
                    $view->SetController($error404);
                    $view->Show();
                }
            }
        }
    }

}