<?php

namespace core;

class Config
{
    const CONFIG_PATH = ROOT . '/config/app.php';

    private static $configArr;

    public static function getConfigByKey($key)
    {
        if (!self::$configArr) {
            self::$configArr = require_once static::CONFIG_PATH;
        }
        return self::$configArr[$key] ?? null;
    }
}
