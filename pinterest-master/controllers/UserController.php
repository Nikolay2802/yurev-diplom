<?php

namespace controllers;

use core\ActiveUser;
use core\ControllerInterface;
use core\View;
use db\UserDB;
use models\User;

class UserController implements ControllerInterface
{
    public function getLayout()
    {
        return 'main';
    }

    public function actionLogout()
    {
        ActiveUser::DeleteActiveUser();
        Header("Location: /");
    }

    public function actionRegistration()
    {
        if (isset($_POST['btn_reg'])) {
            $errors = $this->checkCorrectRegistration($_POST);
            if (empty($errors)) {
                $this->registrationUser();
                Header("Location: /");
            } else {
                return new View('registration', ['errors' => $errors]);
            }
        }
        if (ActiveUser::CheckActiveUser())
            return new View('registration', ['message' => 'Необходимо выйти, чтобы зарегистрироваться!']);
        else
            return new View('registration');
    }

    public function actionLogin()
    {
        if (isset($_POST['btn_login'])) {
            $errors = array();
            $login = $_POST['login'];
            $password = $_POST['pass'];

            if (User::LoginExistInDb($login)) {
                $user = User::GetUserByLogin($login);
                if ($user->password == $password) {
                    ActiveUser::SetActiveUser($user);
                    Header("Location: /");
                } else {
                    $errors[] = "Не верный логин или пароль.";
                    return new View('login', ['errors' => $errors]);
                }
            } else {
                $errors[] = "Не верный логин или пароль.";
                return new View('login', ['errors' => $errors]);
            }
        }
        if (ActiveUser::CheckActiveUser())
            return new View('login', ['message' => 'Вы уже вошли!']);
        else
            return new View('login');
    }

    public function actionEdit()
    {
        if (!ActiveUser::CheckActiveUser()) {
            $errors[] = "Необходимо войти.";
            return new View('login', ['errors' => $errors]);
        }
        if (isset($_POST['submit'])) {
            unset($_POST['submit']);
            User::UpdateUserInfo(ActiveUser::GetActiveUser(), $_POST);
            return new View('edit', ['user' => ActiveUser::GetActiveUser(), 'message' => 'Данные успешно сохранены']);
        }
        return new View('edit', ['user' => ActiveUser::GetActiveUser()]);
    }

    public function actionDelete()
    {
        if (ActiveUser::CheckActiveUser()) {
            $user = ActiveUser::GetActiveUser();
            UserDB::Instance()->DeleteUser($user);
            ActiveUser::DeleteActiveUser();
        }
        return new View('login', ['message' => 'Пользователь успешно удален']);
    }

    private function registrationUser()
    {
        $user = new User(null, $_POST['login'], $_POST['pass'], BASIC_AVATAR_PATH);
        User::AddUserToDb($user);
        $registeredUser = User::GetUserByLogin($_POST['login']);
        ActiveUser::SetActiveUser($registeredUser);
    }

    private function checkCorrectRegistration($registration_array)
    {
        $errors = array();

        if (!isset($registration_array['captchaResult']) || $registration_array['captchaResult'] == 0) {
            $errors[] = 'Неверная капча!';
        }

        if (User::LoginExistInDb($registration_array['login']))
            $errors[] = "Такой логин уже зарегестрирован.";

        $loginLen = strlen($registration_array['login']);
        if ($loginLen < 3 || $loginLen > 24)
            $errors[] = "Логин неправильной длины. (4-20 символов)";

        if ($registration_array['pass'] != $registration_array['pass_again'])
            $errors[] = "Пароли не совпадают.";

        $passLen = strlen($registration_array['pass']);
        if ($passLen < 4 || $passLen > 24)
            $errors[] = "Пароль неправильной длины. (5-20 символов)";

        if (!isset($registration_array['btn_reg']))
            $errors[] = "Неизвестная ошибка. Попробуйте еще раз.";

        return $errors;
    }
}