<?php

namespace controllers;

use core\ActiveUser;
use core\ControllerInterface;
use core\View;
use models\Category;
use models\Comment;
use models\Favorites;
use models\Image;
use models\Likes;

class ImageController implements ControllerInterface
{
    public function getLayout()
    {
        return 'main';
    }

    public function actionIndex()
    {
        $imagesList = Image::GetAllImages();
        return new View('list', [
            'imagesList' => $imagesList,
            'header' => 'Лента',
        ]);
    }

    public function actionSearch()
    {
        if (isset($_POST['search_btn'])) {
            $search_str = $_POST['search_str'];
            $images = Image::FindImages($search_str);
            return new View('list', [
                'imagesList' => $images,
                'header' => "Найденные $search_str",
            ]);
        }
    }

    public function actionAdd()
    {
        if (!ActiveUser::CheckActiveUser())
            return new View('edit', ['errors' => ['' => 'Необходимо войти чтобы добавить пост!']]);

        if (isset($_POST['add_image_btn'])) {
            $errorResult = $this->processErrors();
            if ($errorResult != null)
                return $errorResult;

            $uploadDir = IMAGES_PATH . ActiveUser::GetActiveUser()->login;
            if (!is_dir(ROOT . $uploadDir))
                mkdir(ROOT . $uploadDir);
            $extension = pathinfo(basename($_FILES['image']['name']))['extension'];
            $uploadFilePath = $uploadDir . '/' . time() . '.' . strtolower($extension);
            if (move_uploaded_file($_FILES['image']['tmp_name'], ROOT . $uploadFilePath)) {
                $image = new Image(null, $_POST['title'], $_POST['description'], ActiveUser::GetActiveUser()->id,
                    date("Y-m-d"), $uploadFilePath, mb_strtolower($_POST['category_name']));
                Image::WriteImage($image);
                $imageID = Image::GetLastId();
                $newUploadFilePath = $uploadDir . '/' . $imageID . '.' . strtolower($extension);
                rename(ROOT . $uploadFilePath, ROOT . $newUploadFilePath);
                Image::UpdateImagePath($imageID, $newUploadFilePath);
                Header('Location: /images/' . $imageID);
            } else {
                return new View('edit', ['errors' => ['' => 'Что-то пошло не так. Попробуйте еще раз.']]);
            }
        }
        $categories = Category::GetAllCategories();
        return new View('edit', ['categories' => $categories]);
    }

    public function actionDelete($imageId)
    {
        $image = Image::GetImage($imageId);
        $imagePath = $image->image_path;
        unlink(ROOT .$imagePath);
        Image::DeleteImageById($imageId);
        Header('Location: /');
    }

    private function processErrors()
    {
        if ($_FILES['image']['type'] != "image/png" && $_FILES['image']['type'] != "image/jpeg")
            return new View('edit', ['errors' => ['' => 'Необходимо загружать только изображения!']]);
        if (strlen($_POST['description']) > 4096)
            return new View('edit', ['errors' => ['' => 'Слишком длинное описание']]);
        if (strlen($_POST['title']) < 3)
            return new View('edit', ['errors' => ['' => 'Слишком короткое название']]);
        if (strlen($_POST['category_name']) < 3)
            return new View('edit', ['errors' => ['' => 'Слишком короткое название категории']]);
        return null;
    }

    public function actionView($image_id)
    {
        Image::increaseViewsCount($image_id);
        $image = Image::GetImage($image_id);
        $userIsActive = ActiveUser::CheckActiveUser();
        $comments = Comment::GetCommentsByImageId($image_id);
        $isFavorite = ActiveUser::CheckActiveUser() ? Favorites::isIsset(ActiveUser::GetActiveUser(), $image->id) : false;
        return new View('view', [
            'image' => $image,
            'comments' => $comments,
            'userIsActive' => $userIsActive,
            'isFavorite' => $isFavorite,
            'likesCount' => Likes::getLikesCountForImage($image->id),
        ]);
    }

    public function actionCategory($categoryName)
    {
        $images = Image::GetImagesInCategory(mb_strtolower($categoryName));
        if (!empty($images))
            return new View('list', [
                'imagesList' => $images,
                'header' => "Категория '$categoryName'",
            ]);
        else
            return new View('list', ['error' => 'Пусто.']);
    }

    public function actionUser($login)
    {
        $user = \models\User::GetUserByLogin($login);
        $images = Image::GetUserImages($user->id);
        if (ActiveUser::GetActiveUser()->id == $user->id) {
            return new View('user-admin-list', [
                'imagesList' => $images,
                'header' => "Все изображения $login",
            ]);
        }
        if (!empty($images)) {
            return new View('list', [
                'imagesList' => $images,
                'header' => "Все изображения $login",
            ]);
        }

        return new View('list', ['error' => 'Пусто.']);
    }

    public function actionComment($image_id)
    {
        if (!isset($_POST['comment_btn']))
            return null;
        $comment_text = $_POST['comment_text'];
        $commentParentId = $_POST['comment_parent_id'] ?? null;
        Comment::WriteComment(ActiveUser::GetActiveUser(), $image_id, $comment_text, $commentParentId);
        $result = \models\Mail::sendMailNotificationToImageOwner($image_id);
        Header("Location: /images/$image_id");
    }

    public function actionDeleteComment($image_id, $comment_id)
    {
        Comment::RemoveComment($comment_id);
        Header("Location: /images/$image_id");
    }

    public function actionEdit($imageId)
    {
        $image = Image::GetImage($imageId);
        if (isset($_POST['add_image_btn']) && $image) {
            Image::UpdateImage($imageId, $_POST);
        }
        $image = Image::GetImage($imageId);
        return new View('edit', ['image' => $image]);
    }

    public function actionLike($imageId)
    {
        if (!ActiveUser::CheckActiveUser()) {
            $likesCount = Likes::getLikesCountForImage($imageId);
        } else {
            $likesCount = Likes::likeImage($imageId);
        }
        return new View('like_count', ['likes' => $likesCount], false);
    }

}
