<?php

namespace controllers;

use core\ActiveUser;
use core\ControllerInterface;
use core\View;
use models\Favorites;
use models\Image;

class FavoritesController implements ControllerInterface
{
    public function getLayout()
    {
        return 'main';
    }

    public function actionAdd($id)
    {
        if (Favorites::isIsset(ActiveUser::GetActiveUser(), $id))
            Favorites::Remove(ActiveUser::GetActiveUser(), $id);
        else
            Favorites::Add(ActiveUser::GetActiveUser(), $id);
        Header("Location: /images/$id");
    }

    public function actionRemove($id)
    {
        Favorites::Remove(ActiveUser::GetActiveUser(), $id);
        Header("Location: /favorites");
    }

    public function actionList()
    {
        if (!ActiveUser::CheckActiveUser())
            return new View('list', ['error' => 'Что бы посмотреть избранные, необходимо войти.']);
        $array = Favorites::GetFavorites(ActiveUser::GetActiveUser());
        if (is_null($array) || empty($array))
            return new View('list', ['error' => 'Пусто.']);

        $images = array();
        foreach ($array as $imageId)
            $images[] = Image::GetImage($imageId);
        return new View('list', ['imagesList' => $images]);
    }
}