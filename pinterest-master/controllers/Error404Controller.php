<?php

namespace controllers;

use core\ControllerInterface;
use core\View;

class Error404Controller implements ControllerInterface
{
    public function getLayout()
    {
        return 'main';
    }

    public function actionIndex()
    {
        return new View('404');
    }
}